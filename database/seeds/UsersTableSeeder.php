<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create admin user if not exists
        $adminUser = User::where(['email' => 'admin@example.com'])->first();
        if (!$adminUser) {
            $this->command->info('Creating admin user...');
            factory(User::class)->create([
              'email' => 'admin@example.com',
              'name' => 'admin',
            ]);
            $this->command->info('Done!');
        }

        if(User::count() <= 6) {
            $this->command->info('Creating 5 more users...');
            factory(User::class)->times(5)->create();
            $this->command->info('Done!');
        }
    }
}
