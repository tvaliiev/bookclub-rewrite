include .env

DC_LOCAL=USER=$(shell id -u):$(shell id -g) docker-compose -f docker-compose.yml -f docker-compose.local.yml
PHP=$(DC_LOCAL) exec webserver
PHP_COMPOSER=$(PHP) composer
PHP_ARTISAN=$(PHP) php artisan
NODEJS=$(DC_LOCAL) exec nodejs
NPM=$(NODEJS) npm

rd: run-development
run-development: up-development-environment run-watcher

ud: up-development-environment
up-development-environment:
	$(DC_LOCAL) up -d \
		webserver db nodejs adminer

init: up-development-environment
ifeq (,$(wildcard ./.env))
	cp .env.example .env
endif
	$(PHP_COMPOSER) global require hirak/prestissimo
	$(MAKE) install
	$(PHP_ARTISAN) key:generate

i: install
install:
	$(PHP_COMPOSER) install
	$(NODEJS) npm install

update:
	$(PHP_COMPOSER) update
	$(NPM) upgrade

down:
	$(DC_LOCAL) down

remove-all:
	$(DC_LOCAL) down -v

rw: run-watcher
run-watcher:
	$(NPM) run watch

migrate-seed:
	$(PHP_ARTISAN) migrate:fresh --seed

#rd: run-development
#run-development: run-watcher-container
#	.bin/nodejs yarn run development
#
#run-watcher-container:
#	UID=$(shell id -u):$(shell id -g) docker-compose up -d nodejs
#
#uts: up-test-services
#up-test-services:
#	UID=$(shell id -u):$(shell id -g) docker-compose up -d \
#    --scale chrome=3 \
#		webserver selenium-hub chrome
#
#test: up-test-services
#	.bin/php vendor/bin/phpunit
#	@rm tests/Browser/screenshots/*.png || echo 'No screenshots'
#	.bin/artisan dusk

